function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.

    let cache = new Set()

    function invokeCB(arg1) {

        if (!cache.has(arg1)) {
            cache.add(arg1)
            return cb(arg1)
        } else {
            //console.log("cb with arg= " + arg1 + " already invoked")
            return cache
        }

    }

    return {
        invokeCB
    }
}



module.exports = cacheFunction