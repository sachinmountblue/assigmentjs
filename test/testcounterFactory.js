const counterFactory = require("../counterFactory.js");

var counter = counterFactory();

console.log(counter.value());

counter.increment();
counter.increment();
counter.increment();
console.log(counter.value());

counter.decrement();
counter.decrement();
console.log(counter.value());