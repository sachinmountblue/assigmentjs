const cacheFunction = require("../cacheFunction.js");



let argumentPassed = cacheFunction(cb);
console.log(argumentPassed.invokeCB("a"));
console.log(argumentPassed.invokeCB("b"));
console.log(argumentPassed.invokeCB("4"));
console.log(argumentPassed.invokeCB("a"));
console.log(argumentPassed.invokeCB("f"));


//callback function
function cb(arg1) {
    return ("cb is invoked with arg: " + arg1 + "")
}