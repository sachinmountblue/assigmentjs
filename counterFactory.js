function counterFactory() {
    let counter = 0;

    function changeBy(val) {
        counter += val;
    }
    return {
        increment: function () {
            changeBy(1);
        },

        decrement: function () {
            changeBy(-1);
        },

        value: function () {
            return counter;
        }
    }
}


module.exports = counterFactory