function limitFunctionCallCount(callback, invokeLimit) {

    let callCounter = invokeLimit

    function invokeCB() {


        if (callCounter > 0) {
            callback()
        } else {
            return null
            console.log("You can not invoke callback function, please change invokeLimit if you want to invoke callback")
        }
        callCounter--

    }

    return {
        invokeCB
    }


}




module.exports = limitFunctionCallCount